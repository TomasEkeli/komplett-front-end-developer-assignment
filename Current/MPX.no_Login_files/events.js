﻿define(["Common/pubsub"], function (pubsub) {

    var events = [
        function AlertAdded(message, alertLevel) {
            this.message = message || "";
            this.alertLevel = alertLevel || "";
        },
        function AlertArrayAdded(alertArray) {
            this.alertArray = alertArray;
        },
        function PopupToggled(showPopup, url, popupViewModel, name) {
            this.showPopup = showPopup;
            this.url = url;
            this.popupViewModel = popupViewModel;
            this.name = name;
        }
    ];
    
    return pubsub.extend(events);

});