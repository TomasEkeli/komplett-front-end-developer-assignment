﻿define([], function () {

    function SearchBarViewModel() {

        var self = this;

        this.searchResponse = function (request, response) {

            if (request.term.length > 2) {
                $.ajax({
                    dataType: "json",
                    url: "/Search/Autocomplete",
                    data: { format: "json", q: request.term },
                    success: function (data) {
                        var filterData = self.searchSuccess(data);
                        response(filterData);
                    }
                });
            }
        };

        this.searchSuccess = function (data) {

            var suggestionsTemp = [], label;

            for (var i = 0; i < data.suggestions.length; i++) {
                data.suggestions[i].suggestions.forEach(function (item) {
                    label = self.removeIdsFromFacets(item.label);
                    suggestionsTemp.push({ label: label, value: label });
                });
            }

            return suggestionsTemp;
        };

        this.removeIdsFromFacets = function(value) {
            var spacedItems, underscoredItems, replaceValue;
            spacedItems = value.split(" ");
            for (var i = 0; i < spacedItems.length; i++) {
                underscoredItems = spacedItems[i].split("_");
                if (underscoredItems.length > 1) {
                    replaceValue = underscoredItems[0];
                    if (!isNaN(parseFloat(replaceValue)) && isFinite(replaceValue)) {
                        value = value.replace(replaceValue + "_", "");
                    };
                };
            };
            return value;
        };

    };

    return SearchBarViewModel;
});